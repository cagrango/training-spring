package com.digitalbanking.training.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreditDto {

    private int id;

    private String customerName;

    private double amount;

    private int term;

    private double interestPercent;
    
    private double monthQuota;

}