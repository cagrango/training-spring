package com.digitalbanking.training.persistence;

import com.digitalbanking.training.entities.CreditDto;
import com.digitalbanking.training.interfaces.ICreditRepository;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CreditRepository implements ICreditRepository {

    private List<CreditDto> credits;

    public CreditRepository() {
        credits = new ArrayList<>();
    }

    public CreditDto insert(CreditDto credit) {
        int maxId = 0;
        for (CreditDto item : credits) {
            if( item.getId() > maxId ) {
                maxId = item.getId();
            }
        }
        credit.setId( maxId + 1 );
        credits.add(credit);
        return credit;
    }

    public CreditDto update(CreditDto credit) {
        for (CreditDto item : credits) {
            if( item.getId()  == credit.getId() ) {
                item.setTerm( credit.getTerm() );
                item.setInterestPercent( credit.getInterestPercent() );
                return item;
            }
        }
        return credit;
    }

    public List<CreditDto> getAll() {
        return credits;
    }
}
