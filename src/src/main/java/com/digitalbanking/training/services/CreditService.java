package com.digitalbanking.training.services;

import com.digitalbanking.training.entities.CreditDto;
import com.digitalbanking.training.interfaces.ICreditRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CreditService {

    ICreditRepository creditRepository;

    @Autowired
    public CreditService(ICreditRepository injectedCreditRepository) {
        creditRepository = injectedCreditRepository;
    }

    public CreditDto calculate(CreditDto request) {
        request.setMonthQuota(
                ( request.getAmount() / request.getTerm() )
                * ( 1.0 + request.getInterestPercent() / 12.0 )
        );
        return request;
    }

    public CreditDto insert(CreditDto credit) {
        return creditRepository.insert(credit);
    }

    public CreditDto update(CreditDto credit) {
        return creditRepository.update(credit);
    }

    public List<CreditDto> getAll() {
        return creditRepository.getAll();
    }

}
