package com.digitalbanking.training.interfaces;

import java.util.List;
import com.digitalbanking.training.entities.CreditDto;

public interface ICreditRepository {
    
    CreditDto insert(CreditDto credit);
    CreditDto update(CreditDto credit);
    List<CreditDto> getAll();
    
}
