package com.digitalbanking.training.controllers;

import com.digitalbanking.training.entities.CreditDto;
import com.digitalbanking.training.services.CreditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CreditController {

    @Autowired
    private CreditService creditService;
    
    @PostMapping("/credit/calculate")
    public CreditDto calculate(@RequestBody CreditDto request) {
        return creditService.calculate(request);
    }

    @PostMapping("/credit")
    public CreditDto insert(@RequestBody CreditDto request) {
        return creditService.insert(request);
    }

    @PutMapping("/credit")
    public CreditDto update(@RequestBody CreditDto request) {
        return creditService.update(request);
    }

    @GetMapping("/credit")
    public List<CreditDto> getAll() {
        return creditService.getAll();
    }

}
