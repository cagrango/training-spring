package com.digitalbanking.training.controllers;

import com.digitalbanking.training.entities.LoginRequest;
import com.digitalbanking.training.entities.LoginResponse;
import com.digitalbanking.training.security.TokenUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthorizationController {

    @Autowired
    private TokenUtil tokenUtil;

    @PostMapping("/login")
    public LoginResponse login(@RequestBody LoginRequest request) {
        LoginResponse loginResponse = new LoginResponse();
        loginResponse.setToken( tokenUtil.createJwtToken(request.getLogin()) );
        return loginResponse;
    }

}
