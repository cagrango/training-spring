package com.digitalbanking.training.security;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

import javax.crypto.SecretKey;
import javax.servlet.http.HttpServletRequest;

@Component
public class TokenUtil {
    
    public static final long JWT_TOKEN_VALIDITY = 5 * 60 * 60;

    public static final String JWT_TOKEN_HEADER = "Authorization";

    public static final String JWT_TOKEN_PREFIX = "Bearer";

    private static final String SECRET = "0000000001000000000100000000010000000001000000000100000000010000000001000000000100000000010000000001";

    public String createJwtToken(String userName)
    {
        Map<String, Object> claims = new HashMap<>();

        SecretKey key = Keys.hmacShaKeyFor(SECRET.getBytes());

        String jws = Jwts.builder()
            .setClaims(claims)
            .setSubject(userName)            
            .setIssuedAt(new Date(System.currentTimeMillis()))
            .setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY * 1000))
            .signWith(key,SignatureAlgorithm.HS512).compact();

        return jws;        
    }

    public String extractToken(HttpServletRequest request) {
        final String requestTokenHeader = request.getHeader(JWT_TOKEN_HEADER);
        String jwtToken = null;
        if (requestTokenHeader != null && requestTokenHeader.startsWith(JWT_TOKEN_PREFIX)) {
            jwtToken = requestTokenHeader.substring(7);
        }
        return jwtToken;
    }

    private Claims getAllClaimsFromToken(String jwtToken) {
        SecretKey key = Keys.hmacShaKeyFor(SECRET.getBytes());
        return Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(jwtToken).getBody();
    }

    private <T> T getClaimFromToken(String jwtToken, Function<Claims, T> claimsResolver) {
        final Claims claims = getAllClaimsFromToken(jwtToken);
        return claimsResolver.apply(claims);
    }

    public boolean validateToken(String jwtToken) {
        final Date expirationDate = getExpirationDateFromToken(jwtToken);
        final Date actualDate = new Date(System.currentTimeMillis());
        if( expirationDate.before(actualDate) ) {
            return false;
        }
        else {
            return true;
        }
	}

    public String getUsernameFromToken(String jwtToken) {
        return getClaimFromToken(jwtToken, Claims::getSubject);
    }

    //retrieve expiration date from jwt token
    private Date getExpirationDateFromToken(String jwtToken) {
        return getClaimFromToken(jwtToken, Claims::getExpiration);
    }    

}
