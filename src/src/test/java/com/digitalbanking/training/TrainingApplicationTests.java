package com.digitalbanking.training;

import com.digitalbanking.training.entities.CreditDto;
import com.digitalbanking.training.interfaces.ICreditRepository;
import com.digitalbanking.training.services.CreditService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@SpringBootTest
public class TrainingApplicationTests {
    
    private CreditService creditService;

    private static CreditDto credit;

    public static CreditDto GetTestCredit() {
        if (credit == null) {
            credit = new CreditDto(0, "Test", 12000, 12, 2.4, 0);
        }
        return credit;
    }

    @BeforeEach
    public void prepare() {
        ICreditRepository repository = Mockito.mock(ICreditRepository.class);
        creditService = new CreditService(repository);
        when(repository.getAll()).thenReturn(new ArrayList<>());
        when(repository.insert(GetTestCredit())).thenReturn(GetTestCredit());
        when(repository.update(GetTestCredit())).thenReturn(GetTestCredit());
    }

    @Test
    public void contextLoads() {
        assertThat(creditService).isNotNull();
    }

    @Test
    public void getAll() {
        assertThat(creditService.getAll()).hasSize(0);
    }

    @Test
    public void calculate() {
        assertThat(creditService.calculate(GetTestCredit()).getMonthQuota()).isEqualTo(1200);
    }

    @Test
    public void create() {
        assertThat(creditService.insert(GetTestCredit()).getId()).isEqualTo(GetTestCredit().getId());
    }

    @Test
    public void update() {
        assertThat(creditService.update(GetTestCredit()).getInterestPercent())
                .isEqualTo(GetTestCredit().getInterestPercent());
    }

}
